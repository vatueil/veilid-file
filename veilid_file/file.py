#!/usr/bin/env python

"""Veilid file transfer."""

import argparse
import asyncio
import aiofiles
import logging
import math
import os
import sys
from concurrent.futures import ThreadPoolExecutor
from typing import Optional

import veilid

BLOCK_SIZE = 32768
LOG = logging.getLogger(__name__)

# Helper functions


async def noop_callback(*args, **kwargs):
    return


async def write_chunk(router: veilid.api.RoutingContext, key: veilid.TypedKey, chunk_num: veilid.ValueSubkey, chunk: bytes):
    await router.set_dht_value(key, veilid.ValueSubkey(chunk_num), chunk)


async def put(host: str, port: int, name: str):
    """Put a file into Veilid DHT."""

    filesize = os.path.getsize(name)
    n_chunks = math.ceil(filesize / BLOCK_SIZE)
    LOG.info(f"put:{host=}, {port=}, {name=}, {n_chunks=}")

    LOG.debug(f"put:Connecting to the Veilid API at {host=}, {port=}, {noop_callback}")
    conn = await veilid.json_api_connect(host, port, noop_callback)

    LOG.debug("put:Opening a private routing context")
    router = await (await conn.new_routing_context()).with_privacy()
    async with router:
        LOG.debug("put:Creating a new DHT record")
        record = await router.create_dht_record(veilid.DHTSchema.dflt(n_chunks))
        record_key = record.key

        async with aiofiles.open(name, 'rb') as f:
            for chunk_num in range(n_chunks):
                chunk = await f.read(BLOCK_SIZE)
                LOG.debug("put:Writing DHT {record_key=} subkey {chunk_num=}")
                await write_chunk(router, record.key, chunk_num, chunk)

        LOG.debug("put:Closing DHT record")
        await router.close_dht_record(record.key)

    print(f"{record_key}")


async def get(host: str, port: int, key: str, name: Optional[str] = None):
    """Get a file from Veilid DHT."""

    LOG.info(f"get:{host=}, {port=}, {name=}, {key=}")

    LOG.debug(f"get:Connecting to the Veilid API at {host=}, {port=}, {noop_callback}")
    conn = await veilid.json_api_connect(host, port, noop_callback)

    LOG.debug("get:Opening a private routing context")
    router = await (await conn.new_routing_context()).with_privacy()
    async with router:
        LOG.debug(f"get:Opening the DHT record {key=}")
        await router.open_dht_record(key, None)

        async with aiofiles.open(name, 'wb') as f:
            chunk_num = 0
            try:
                while True:
                    LOG.debug("put:Reading DHT {key=} subkey {chunk_num=}")
                    resp = await router.get_dht_value(key, chunk_num, True)
                    if not resp or not resp.data:
                        return
                    await f.write(resp.data)
                    if len(resp.data) < BLOCK_SIZE:
                        return
                    chunk_num += 1
            finally:
                LOG.debug(f"get:Closing the DHT record {key=}")
                await router.close_dht_record(key)


async def delete(host: str, port: int, key: str):
    """Delete a file from DHT."""

    LOG.info(f"delete:{host=}, {port=}, {key=}")

    LOG.debug(f"delete:Connecting to the Veilid API at {host=}, {port=}, {noop_callback}")
    conn = await veilid.json_api_connect(host, port, noop_callback)

    LOG.debug("delete:Opening a non-private routing context")
    router = await conn.new_routing_context()
    async with router:
        LOG.debug(f"delete:Deleting the DHT record {key=}")
        await router.delete_dht_record(key)


# Command line processing


def handle_command_line(arglist: Optional[list[str]] = None):
    """Process the command line.

    This isn't the interesting part."""

    if arglist is None:
        arglist = sys.argv[1:]

    parser = argparse.ArgumentParser(description="Veilid chat demonstration")
    parser.add_argument("--host", default="localhost", help="Address of the Veilid server host.")
    parser.add_argument("--port", type=int, default=5959, help="Port of the Veilid server.")
    parser.add_argument("--verbose", "-v", default=0, action="count")

    subparsers = parser.add_subparsers(required=True)

    # Chat commands

    cmd_put = subparsers.add_parser("put", help=put.__doc__)
    cmd_put.add_argument("name", help="filename")
    cmd_put.set_defaults(func=put)

    cmd_get = subparsers.add_parser("get", help=get.__doc__)
    cmd_get.add_argument("key", help="file DHT key")
    cmd_get.add_argument("name", default="/dev/stdout", nargs='?', help="filename")
    cmd_get.set_defaults(func=get)

    cmd_delete = subparsers.add_parser("delete", help=delete.__doc__)
    cmd_delete.add_argument("key", help="file DHT key to delete")
    cmd_delete.set_defaults(func=delete)

    args = parser.parse_args(arglist)
    kwargs = args.__dict__
    func = kwargs.pop("func")

    verbose = kwargs.pop("verbose")
    if verbose == 0:
        loglevel = logging.WARNING
    elif verbose == 1:
        loglevel = logging.INFO
    else:
        loglevel = logging.DEBUG

    logging.basicConfig(level=loglevel)

    asyncio.run(func(**kwargs))


if __name__ == "__main__":
    handle_command_line()
