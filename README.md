# Veilid file transfer utility

This project demonstrates simple file transfer and distribution over [Veilid](https://veilid.com).

## Why

This utility demonstrates private, anonymous, but not confidential content distribution. The [chat demo](https://github.com/veilid/python-demo) is cool, but demonstrates a more point-to-point confidential communication method.

veilid-file has a different use case in mind: a way to distribute content such that anyone may receive it, but the publisher remains anonymous.

This tool will most likely be superseded by Veilid's planned content-addressible block storage, but it is a neat way to interact with the network in the meantime.

## Installation

1. Install [poetry](https://python-poetry.org) if you haven't already.
2. Run `poetry install`

## Usage

Run a local [Veilid node](https://gitlab.com/veilid/veilid), such as `veilid-server`. This demo tries to connect to a node's JSON API on localhost port 5959 by default. You can override that with the `--host` and `--port` arguments.

### Put files into Veilid

```
$ poetry run file put myfile.txt
VLD0:76KfMQ7Fhl6l5450m12BbAVlM6g7pOKFN2drNbLLiWo
```

Putting the file in prints its DHT key. Share them!

### Get files out of their Veilid

```
$ poetry run file get VLD0:76KfMQ7Fhl6l5450m12BbAVlM6g7pOKFN2drNbLLiWo
<dumps to stdout>
```

Get files by DHT keys.

Write the output to a file with an extra filename argument:

```
$ poetry run file get VLD0:76KfMQ7Fhl6l5450m12BbAVlM6g7pOKFN2drNbLLiWo yourfile.txt
```

### Delete DHT entries

You can't really delete records from other nodes once they're out on the network, but you can purge them from your local node.

```
$ poetry run file delete VLD0:76KfMQ7Fhl6l5450m12BbAVlM6g7pOKFN2drNbLLiWo
```

## Technical notes

Files are stored in Veilid's DHT using the DFLT protocol.

They're chopped up into chunks of a fixed block size. Each sequential block goes into an incrementing subkey slot.

